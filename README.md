# Setup SOCKS5 proxy with SSH tunnel

### Prerequsites
- A linux box on the LAN (running 7 X 24), __presume its ip address is (192.168.1.47), you should replace it with actual ip address on the LAN.__
- An account to connect to a remote linux box, which is on the other side of the wormhole.

### Install Packages on Local Linux Box

- autossh
- nginx *(or any other http server)*

```sh
sudo apt-get install autossh nginx
```

### Start SSH Port Forwarding

Run the following command on local linux box to start SSH port forwarding as a background process.

```
autossh -M 0 -4 -N -f -o "ServerAliveInterval 60" -o "ServerAliveCountMax 3" -D 0.0.0.0:7072 user@remote-linux-box
```

You can use the following command to verify the connectivity to its 7072 port __from any other machine__ on the LAN

```
telnet 192.168.1.47 7072
```

### Write Your Proxy Auto Configuration File

Create proxy auto configuration file at `/var/www/html/proxy.pac` (for nginx) with following content.

```javascript
function FindProxyForURL(url, host) {
  proxy = "SOCKS5 192.168.1.47:7072; SOCKS 192.168.1.47:7072"

  // direct connect for any simple host names or intranet ip address.
  if (isPlainHostName(host) ||
      shExpMatch(host, "*.local") ||
      isInNet(dnsResolve(host), "10.0.0.0", "255.0.0.0") ||
      isInNet(dnsResolve(host), "172.16.0.0",  "255.240.0.0") ||
      isInNet(dnsResolve(host), "192.168.0.0",  "255.255.0.0") ||
      isInNet(dnsResolve(host), "127.0.0.0", "255.255.255.0"))
    return "DIRECT";

  // direct connect for some specific domains.
  if (shExpMatch(host, "*.tencent.com") ||
      shExpMatch(host, "*.qq.com"))
    return "DIRECT";

  // default choice.
  return proxy;
}
```

### Configure Your Apps

In system/browser proxy settings, choose automatic proxy configuration and set URL of .pac file as `http://192.168.1.47/proxy.pac`

To use this proxy for command line tools, set environment variables as following:

```sh
export HTTP_PROXY=socks5://192.168.1.47:7072
export HTTPS_PROXY=socks5://192.168.1.47:7072
```

### Troubleshooting

In Google Chrome, you can open the following internal page to see what proxy the Chrome is actually using and force it to reload the pac file. Sometimes when Chrome having problem to get or parse the pac file, it will fallback to use 'DIRECT' for all urls.

[chrome://net-internals/#proxy](chrome://net-internals/#proxy)


